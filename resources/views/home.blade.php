@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Panel de Administración</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    <p>
                        <span id= "products-total">{{$products->total()}} Registros</span>    |
                        Página {{$products->currentPage()}}
                        de {{$products->lastPage()}}
                    </p>
                    <div id = "alert" class="alert alert-info"></div>
                    <table class= "table table-hover">
                        <thead class="thead-dark">
                            <tr>
                                <th width="20px"> ID</th>
                                <th>Nombre</th>
                                <th>Eliminar</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($products as $product)
                            <tr>
                                <td width="20px">{{$product->id }}</td>
                                <td> {{$product->name}}</td>
                                <td width="20px">
                                    {!! Form::open(['route'=>['destroyProduct',$product->id],'method'=>'DELETE'])!!}

                                        <a href="" class = "btn btn-danger btn-delete"> eliminar</a>

                                    {!! Form::close()!!}
                                </td>
                            </tr>
                            @endforeach
                        </tbody>               
                    </table>
                    {!! $products->render()!!}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection


@section('script')
    <script src="{{asset('js/script.js')}}"></script>
@endsection
